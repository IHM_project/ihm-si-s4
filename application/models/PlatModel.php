<?php
class PlatModel extends CI_Model {
	private $idPlat;
	private $nom;
	private $categorie;
	private $prix;
	private $disponible;
	private $nomCategorie;

	public function hydrate(array $data) {
		foreach($data as $key => $value) {
			$this->__set($key,$value);
		}
	}

	public function __get($property) {
		if (property_exists($this, $property)) {
			return $this->$property;
		}
	}

	public function __set($property, $value) {
		if (property_exists($this, $property)) {
			$this->$property = $value;
		}
		return $this;
	}

	public function platsByCateg($plats){
		$categ = array();
		$temp = array();
		for($i = 0; $i < count($plats); $i++){
			if(in_array($plats[$i]->nomCategorie, $temp))
				continue;
			$temp[] = $plats[$i]->nomCategorie;
			$platBytcateg = array();
			for($j = 0; $j < count($plats); $j++){
				if($plats[$j]->nomCategorie == $plats[$i]->nomCategorie)
					$platBytcateg[] = $plats[$j];
			}
			$categ[] = $platBytcateg;
		}
		return $categ;
	}
}
?>