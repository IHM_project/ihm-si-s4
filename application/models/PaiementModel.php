<?php
class PaiementModel extends CI_Model {
	private $idPaiement;
	private $commande;
	private $prix;

	public function hydrate(array $data) {
		foreach($data as $key => $value) {
			$this->__set($key,$value);
		}
	}

	public function __get($property) {
		if (property_exists($this, $property)) {
			return $this->$property;
		}
	}

	public function __set($property, $value) {
		if (property_exists($this, $property)) {
			$this->$property = $value;
		}
		return $this;
	}
}
?>