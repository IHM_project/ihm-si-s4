<?php
class UserModel extends CI_Model {
	private $idUtilisateur;
	private $nom;
	private $prenom;
	private $sexe;
	private $email;
	private $motDePasse;
	private $profil;
	private $nomProfil;

	public function hydrate(array $data) {
		foreach($data as $key => $value) {
			$this->__set($key,$value);
		}
	}

	public function __get($property) {
		if (property_exists($this, $property)) {
			return $this->$property;
		}
	}

	public function __set($property, $value) {
		if (property_exists($this, $property)) {
			$this->$property = $value;
		}
		return $this;
	}
}
?>