<?php
class CommandeCtrl extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->helper("form");
		$this->load->model('UserModel');
		$this->load->library("session");
		$this->load->database();
	}

	public function payer() {
		$commande = trim($this->input->post("commande"));
		
		$sql = "select sum(p.prix) as prixTotal from commande as c join plat as p on c.plat = p.idPlat where numCommande like %s";
		$sql = sprintf($sql,$commande);
		$query = $this->db->query($sql);
		$row = $query->result_array();
		$prix = $row[0]["prixTotal"];
		$sql = "insert into paiement(commande, prix) values (%s,%s)";
		$sql = sprintf($sql,$commande,$prix);
		$this->db->query($sql);
	}

	public function insertCommande() {
		$table = trim($this->input->post("table"));
		$plat = trim($this->input->post("plat"));
		$nombre = trim($this->input->post("nombre"));
		$serveur = trim($this->session->userdata("idUtilisateur"));
		$numCommande = $this->getNumCommande($table);
		$sql = "insert into commande(numCommande, serveur, plat, nombre, tables) values (%s,%s,%s,%s,%s)";
		$sql = sprintf($sql,$numCommande,$serveur,$plat,$nombre,$table);
		$this->db->query($sql);
	}

	public function getNumCommande($table) {
		$sql = "select * from commande as c where tables like %s and c.numCommande not in (select commande from paiement)";
		$sql = sprintf($sql,$table);
		$query = $this->db->query($sql);
		$row = $query->result_array();
		if(!empty($row)) {
			return $row[0]["numCommande"];
		}
		$sql = "insert into numCommande values()";
		$this->db->query($sql);
		$sql = "select * from numCommande order by idNumCommande DESC LIMIT 1";
		$query = $this->db->query($sql);
		$row = $query->result_array();
		return $row[0]["idNumCommande"];
	}
}
?>