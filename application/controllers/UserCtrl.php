<?php
class UserCtrl extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->helper("form");
		$this->load->model('UserModel');
		$this->load->library("session");
	}

	public function index() {
		$this->load->view('UserView');
	}

	public function deconnection() {
		if(($this->session->has_userdata("idUtilisateur"))) {
			$this->session->sess_destroy();
			$data['view'] = 'signIn';
			$this->load->view('template', $data);
		}
		$data['view'] = 'signIn';
		redirect('UserCtrl/signIn');
	}

	
	
	public function signIn(){
		$data['view'] = 'signIn';
		$this->load->view('template', $data);
	}

	public function login() {
		$this->load->database();
		$pseudo = trim($this->input->post("nom"));
		$pwd = trim($this->input->post("pass"));
		$sql = "select u.*,p.nom as nomProfil from utilisateur u join profil p on u.profil=p.idProfil where prenom like '%s' and motDePasse like SHA1('%s')";
		$sql = sprintf($sql,$pseudo,$pwd);
		$query = $this->db->query($sql);
		$row = $query->result_array();
		if(!empty($row)) {
			$this->session->set_userdata($row[0]);
			$data['view'] = 'home';
			$this->load->view('template', $data);
		} else {
			throw new Exception("Login or password invalid", 1);
		}
	}
}
?>