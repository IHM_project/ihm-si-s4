<?php
class PlatCtrl extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->helper("form");
		$this->load->helper("url");
		$this->load->model("PlatModel");
		$this->load->database();
	}

	public function index() {
	}

	public function listeCategorie() {
		$query = $this->db->query("select * from categoriePlat");
		$result = array();
		foreach ($query->result_array() as $row) {
			$result[] = $row;
		}
		return $result;
	}

	public function insert() {
		$categorie = trim($this->input->post("categorie"));
		$nom = trim($this->input->post("nomPlat"));
		$prix = trim($this->input->post("prix"));

		$sql = "insert into plat (nom, categorie, prix, disponible) values ('%s', %s, %s,1);";
		$sql = sprintf($sql,$nom,$categorie,$prix);
		$this->db->query($sql);
	}

	public function update() {
		$idPlat = trim($this->input->post("idPlat"));
		$prix = trim($this->input->post("prix"));
		$disponibilite = trim($this->input->post("dispo"));
		$sql = "update plat set prix = %s and disponible = %s where idPlat = %s";
		$sql = sprintf($sql,$prix,$disponibilite,$idPlat);
		$this->db->query($sql);
	}

	public function delete() {
		$idPlat = trim($this->input->post("idPlat"));
		$sql = "delete from plat where idPlat = %s";
		$sql = sprintf($sql,$idPlat);
		$this->db->query($sql);
	}

	public function search() {
		$categorie = trim($this->input->post("categorie"));
		$name = trim($this->input->post("nomPlat"));
		$sql = "select * from platDispo where nomCategorie like '%s' and nom like '%s%s%s'";
		$sql = sprintf($sql,$categorie,"%",$name,"%");
		$query = $this->db->query($sql);
		$result = array();
		foreach ($query->result_array() as $row) {
			$temp = new PlatModel();
			$temp->hydrate($row);
			$result[] = $temp;
		}
		return $result;
	}

	public function searchByCategorie() {
		$categorie = trim($this->input->post("categorie"));
		$sql = "select * from platDispo where nomCategorie like '%s'";
		$sql = sprintf($sql,$categorie);
		$query = $this->db->query($sql);
		$result = array();
		foreach ($query->result_array() as $row) {
			$temp = new PlatModel();
			$temp->hydrate($row);
			$result[] = $temp;
		}
		return $result;
	}

	public function searchByName() {
		$name = trim($this->input->post("nomPlat"));
		$sql = "select * from platDispo where nom like '%s%s%s'";
		$sql = sprintf($sql,"%",$name,"%");
		$query = $this->db->query($sql);
		$result = array();
		foreach ($query->result_array() as $row) {
			$temp = new PlatModel();
			$temp->hydrate($row);
			$result[] = $temp;
		}
		return $result;
	}

	public function research(){
		$result = [];
		if($this->input->post("nomPlat")!='' && $this->input->post("categorie")==''){
			$result = $this->searchByName();
		}else if($this->input->post("nomPlat")=='' && $this->input->post("categorie")!=''){
			$result = $this->searchByCategorie();
		}else{
			$result = $this->search();
		}
		$data['categories'] = $this->listeCategorie();
		$data['view']='menu';
		$data['plats']=$result;
		$this->load->view("template",$data);
	}

	public function listePlats() {
		$query = $this->db->query("select * from platDispo");
		$result = array();
		foreach ($query->result_array() as $row) {
			$temp = new PlatModel();
			$temp->hydrate($row);
			$result[] = $temp;
		}
		$data['categories'] = $this->listeCategorie();
		$data['view']='menu';
		$data['plats']=$result;
		$this->load->view("template",$data);
	}
}
?>