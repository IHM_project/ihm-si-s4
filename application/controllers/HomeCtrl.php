<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeCtrl extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
        $data['view'] = 'home';
		$this->load->view('template', $data);
	}

	public function menu(){
		$this->load->model('PlatModel');
		$data['view'] = 'menu';
		$this->load->view('template', $data);
	}
}
