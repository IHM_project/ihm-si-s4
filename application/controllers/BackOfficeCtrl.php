<?php
class BackOfficeCtrl extends CI_Controller {
	public function __construct() {
		parent::__construct();
	}

	public function backOffice() {
        $data['view'] = 'backOffice';
        $this->load->view('template', $data);
    }
}
?>