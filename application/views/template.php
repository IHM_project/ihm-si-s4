<?php
    $menuLink = base_url().'PlatCtrl/listePlats';
    if($this->session->userdata('nomProfil') == 'admin')
        $menuLink = base_url().'PlatCtrl/listePlats';
    if($this->session->userdata('nomProfil') != null){
        $linkSingOutIn = base_url().'signOut';
        $sign = 'SignOut';
    }else{
        $linkSingOutIn = base_url().'signIn';
        $sign = 'Sign In';
        $view = 'signIn';
    }

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Home</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="icon" type="<?php echo base_url(); ?>assets/image/png" href="<?php echo base_url(); ?>assets/images/icons/favicon.png"/>
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/onts/themify/themify-icons.css">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/animate/animate.css">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/slick/slick.css">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/lightbox2/css/lightbox.min.css">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/util.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/main.css">
    <!--===============================================================================================-->
    </head>
    <body class="animsition">
        <!-- Header -->
        <header>
            <!-- Header desktop -->
            <div class="wrap-menu-header gradient1 trans-0-4">
                <div class="container h-full">
                    <div class="wrap_header trans-0-3">

                        <!-- Menu -->
                        <div class="wrap_menu p-l-45 p-l-0-xl">
                            <nav class="menu">
                                <ul class="main_menu">
                                    <li>
                                        <a href="<?php echo base_url(); ?>index">Home</a>
                                    </li>

                                    <li>
                                        <a href="<?php echo base_url(); ?>listePlats">Menu</a>
                                    </li>
                                <?php if($this->session->userdata('nomProfil') == 'admin'){?>
                                    <li>
                                        <a href="<?php echo base_url(); ?>backOffice">back office</a>
                                    </li>
                                <?php }?>
                                    <li>
                                        <a href="<?php echo $linkSingOutIn; ?>"><?php echo $sign; ?></a>
                                    </li>
                                </ul>
                            </nav>
                        </div>

                        <!-- Social -->
                        <div class="social flex-w flex-l-m p-r-20">
                            <a href="#"><i class="fa fa-tripadvisor" aria-hidden="true"></i></a>
                            <a href="#"><i class="fa fa-facebook m-l-21" aria-hidden="true"></i></a>
                            <a href="#"><i class="fa fa-twitter m-l-21" aria-hidden="true"></i></a>

                            <button class="btn-show-sidebar m-l-33 trans-0-4"></button>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Sidebar -->
        <aside class="sidebar trans-0-4">
            <!-- Button Hide sidebar -->
            <button class="btn-hide-sidebar ti-close color0-hov trans-0-4"></button>

            <!-- - -->
            <ul class="menu-sidebar p-t-95 p-b-70">
                <li class="t-center m-b-13">
                    <a href="<?php echo base_url(); ?>index" class="txt19">Home</a>
                </li>

                <li class="t-center m-b-13">
                    <a href="<?php echo base_url(); ?>listePlats" class="txt19">Menu</a>
                </li>

                <?php if($this->session->userdata('nomProfil') == 'admin'){?>
                    <li class="t-center m-b-13">
                        <a href="<?php echo base_url(); ?>backOffice" class="txt19">Back Office</a>
                    </li>
                <?php }?>

                <li class="t-center">
                    <!-- Button3 -->
                    <a href="<?php echo $linkSingOutIn; ?>" class="btn3 flex-c-m size13 txt11 trans-0-4 m-l-r-auto">
                        <?php echo $sign; ?>
                    </a>
                </li>
            </ul>

            <!-- - -->
            <div class="gallery-sidebar t-center p-l-60 p-r-60 p-b-40">
                <!-- - -->
                <h4 class="txt20 m-b-33">
                    Gallery
                </h4>

                <!-- Gallery -->
                <div class="wrap-gallery-sidebar flex-w">
                    <a class="item-gallery-sidebar wrap-pic-w" href="<?php echo base_url(); ?>assets/images/photo-gallery-01.jpg" data-lightbox="gallery-footer">
                        <img src="<?php echo base_url(); ?>assets/images/photo-gallery-thumb-01.jpg" alt="GALLERY">
                    </a>

                    <a class="item-gallery-sidebar wrap-pic-w" href="<?php echo base_url(); ?>assets/images/photo-gallery-02.jpg" data-lightbox="gallery-footer">
                        <img src="<?php echo base_url(); ?>assets/images/photo-gallery-thumb-02.jpg" alt="GALLERY">
                    </a>

                    <a class="item-gallery-sidebar wrap-pic-w" href="<?php echo base_url(); ?>assets/images/photo-gallery-03.jpg" data-lightbox="gallery-footer">
                        <img src="<?php echo base_url(); ?>assets/images/photo-gallery-thumb-03.jpg" alt="GALLERY">
                    </a>

                    <a class="item-gallery-sidebar wrap-pic-w" href="<?php echo base_url(); ?>assets/images/photo-gallery-05.jpg" data-lightbox="gallery-footer">
                        <img src="<?php echo base_url(); ?>assets/images/photo-gallery-thumb-05.jpg" alt="GALLERY">
                    </a>

                    <a class="item-gallery-sidebar wrap-pic-w" href="<?php echo base_url(); ?>assets/images/photo-gallery-06.jpg" data-lightbox="gallery-footer">
                        <img src="<?php echo base_url(); ?>assets/images/photo-gallery-thumb-06.jpg" alt="GALLERY">
                    </a>

                    <a class="item-gallery-sidebar wrap-pic-w" href="<?php echo base_url(); ?>assets/images/photo-gallery-07.jpg" data-lightbox="gallery-footer">
                        <img src="<?php echo base_url(); ?>assets/images/photo-gallery-thumb-07.jpg" alt="GALLERY">
                    </a>

                    <a class="item-gallery-sidebar wrap-pic-w" href="<?php echo base_url(); ?>assets/images/photo-gallery-09.jpg" data-lightbox="gallery-footer">
                        <img src="<?php echo base_url(); ?>assets/images/photo-gallery-thumb-09.jpg" alt="GALLERY">
                    </a>

                    <a class="item-gallery-sidebar wrap-pic-w" href="<?php echo base_url(); ?>assets/images/photo-gallery-10.jpg" data-lightbox="gallery-footer">
                        <img src="<?php echo base_url(); ?>assets/images/photo-gallery-thumb-10.jpg" alt="GALLERY">
                    </a>

                    <a class="item-gallery-sidebar wrap-pic-w" href="<?php echo base_url(); ?>assets/images/photo-gallery-11.jpg" data-lightbox="gallery-footer">
                        <img src="<?php echo base_url(); ?>assets/images/photo-gallery-thumb-11.jpg" alt="GALLERY">
                    </a>
                </div>
            </div>
        </aside>

        <?php include('page/'.$view.'.php'); ?>

        <!-- Footer -->
        <footer class="bg1">
            <div class="container p-t-40 p-b-70">
                <div class="row">
                    <div class="col-sm-12 col-md-6 p-t-50">
                        <!-- - -->
                        <h4 class="txt13 m-b-33">
                            Contact Us
                        </h4>

                        <ul class="m-b-70">
                            <li class="txt14 m-b-14">
                                <i class="fa fa-map-marker fs-16 dis-inline-block size19" aria-hidden="true"></i>
                                IT-University
                            </li>

                            <li class="txt14 m-b-14">
                                <i class="fa fa-phone fs-16 dis-inline-block size19" aria-hidden="true"></i>
                                (+1) 96 716 6879
                            </li>

                            <li class="txt14 m-b-14">
                                <i class="fa fa-envelope fs-13 dis-inline-block size19" aria-hidden="true"></i>
                                Resto@gmail.com
                            </li>
                        </ul>

                        <!-- - -->
                        <h4 class="txt13 m-b-32">
                            Opening Times
                        </h4>

                        <ul>
                            <li class="txt14">
                                09:30 AM – 11:00 PM
                            </li>

                            <li class="txt14">
                                Every Day
                            </li>
                        </ul>
                    </div>

                    <div class="col-sm-12 col-md-6 p-t-50">
                        <!-- - -->
                        <h4 class="txt13 m-b-32">
                            Createur
                        </h4>

                        <ul class="m-b-70">
                            <li class="txt14">
                                RAKOTOARIVELO Barinekena - ETU 000826
                            </li>

                            <li class="txt14">
                                HUNALD Marcus Jourdan - ETU 000811
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="end-footer bg2">
                <div class="container">
                    <div class="flex-sb-m flex-w p-t-22 p-b-22">
                        <div class="p-t-5 p-b-5">
                            <a href="#" class="fs-15 c-white"><i class="fa fa-tripadvisor" aria-hidden="true"></i></a>
                            <a href="#" class="fs-15 c-white"><i class="fa fa-facebook m-l-18" aria-hidden="true"></i></a>
                            <a href="#" class="fs-15 c-white"><i class="fa fa-twitter m-l-18" aria-hidden="true"></i></a>
                        </div>

                        <div class="txt17 p-r-20 p-t-5 p-b-5">
                            Copyright &copy; 2019 All rights reserved
                        </div>
                    </div>
                </div>
            </div>
        </footer>


        <!-- Back to top -->
        <div class="btn-back-to-top bg0-hov" id="myBtn">
            <span class="symbol-btn-back-to-top">
                <i class="fa fa-angle-double-up" aria-hidden="true"></i>
            </span>
        </div>

        <!-- Container Selection1 -->
        <div id="dropDownSelect1"></div>

    <!--===============================================================================================-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/animsition/js/animsition.min.js"></script>
    <!--===============================================================================================-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/popper.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/select2/select2.min.js"></script>
    <!--===============================================================================================-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/daterangepicker/moment.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/daterangepicker/daterangepicker.js"></script>
    <!--===============================================================================================-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/slick/slick.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/slick-custom.js"></script>
    <!--===============================================================================================-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/parallax100/parallax100.js"></script>
        <script type="text/javascript">
            $('.parallax100').parallax100();
        </script>
    <!--===============================================================================================-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/countdowntime/countdowntime.js"></script>
    <!--===============================================================================================-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/lightbox2/js/lightbox.min.js"></script>
    <!--===============================================================================================-->
        <script src="<?php echo base_url(); ?>assets/js/main.js"></script>

    </body>
</html>
