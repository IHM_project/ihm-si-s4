	<!-- Title Page -->
	<section class="bg-title-page flex-c-m p-t-160 p-b-80 p-l-15 p-r-15" style="background-image: url(<?php echo base_url(); ?>assets/images/bg-title-page-02.jpg);">
		<h2 class="tit6 t-center">
			Sign In
		</h2>
	</section>


	<!-- SignIn -->
	<section class="section-reservation bg1-pattern p-t-100 p-b-113">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 p-b-30">
					<div class="t-center">
						<span class="tit2 t-center">
							Sign In
						</span>
					</div>

					<form class="wrap-form-reservation size22 m-l-r-auto" method="post" action="<?php echo base_url()?>logIn">
						<div class="col-md-4">
							<!-- Name -->
							<span class="txt9">
								Name
							</span>

							<div class="wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">
								<input class="bo-rad-10 sizefull txt10 p-l-20" type="text" name="nom" placeholder="Name">
							</div>
						</div>

						<div class="col-md-4">
							<!-- Name -->
							<span class="txt9">
								Password
							</span>

							<div class="wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">
								<input class="bo-rad-10 sizefull txt10 p-l-20" type="password" name="pass" placeholder="Password">
							</div>
						</div>

						<div class="wrap-btn-booking flex-c-m m-t-6">
							<!-- Button3 -->
							<button type="submit" class="btn3 flex-c-m size13 txt11 trans-0-4">
								Sign In
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>