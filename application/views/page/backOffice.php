	<!-- Title Page -->
	<section class="bg-title-page flex-c-m p-t-160 p-b-80 p-l-15 p-r-15" style="background-image: url(<?php echo base_url(); ?>assets/images/bg-title-page-03.jpg);">
		<h2 class="tit6 t-center">
			Back Office
		</h2>
	</section>


	<!-- SignIn -->
	<section class="section-reservation bg1-pattern p-t-100 p-b-113">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 p-b-30">
					<div class="t-center">
						<span class="tit2 t-center">
                            Back Office
						</span>
					</div>
				</div>
			</div>
		</div>
	</section>