	<?php
		$listPlats = [];
		$numberList = 0;
		if(isset($plats)){
			$listPlats = $plats;
			$numberList = round(count($categories)/2);
		}
		$listeCateg = array();
		for($i = 0; $i < count($listPlats); $i++){
			$listeCateg[] = $listPlats[$i]->nomCategorie;
		}
	?>
	<!-- Title Page -->
	<section class="bg-title-page flex-c-m p-t-160 p-b-80 p-l-15 p-r-15" style="background-image: url(<?php echo base_url(); ?>assets/images/bg-title-page-01.jpg);">
		<h2 class="tit6 t-center">
			Barea Menu
		</h2>
	</section>

	<section class="section-reservation bg1-pattern p-t-100">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 p-b-30">
					<div class="t-center">
						<span class="tit2 t-center">
							Barea Menu
						</span>
					</div>
					<form class="wrap-form-reservation size22 p-t-50 m-l-r-auto" method="post" action="<?php echo base_url()?>research">
						<div class="row">
							<div class="col-md-4">
								<!-- Date -->
								<span class="txt9">
									Categorie
								</span>

								<div class="wrap-inputtime size12 bo2 bo-rad-10 m-t-3 m-b-23">
									<select class="selection-1" name="categorie">
									<?php for($i = 0; $i < count($categories); $i++){ ?>
										<option><?php echo $categories[$i]['nom'] ?></option>
									<?php } ?>
									</select>
								</div>
							</div>

							<div class="col-md-4">
								<span class="txt9">
									Plat
								</span>
								<div class="wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">
									<input class="bo-rad-10 sizefull txt10 p-l-20" type="text" name="nomPlat" placeholder="Plat">
								</div>
							</div>
							<div class="wrap-btn-booking flex-c-m m-t-6">
								<!-- Button3 -->
								<button type="submit" class="btn3 flex-c-m size13 txt11 trans-0-4">
									Find
								</button>
							</div>
						</div>

					</form>
				</div>
			</div>
	</section>

	<!-- menu -->
	<section class="section-mainmenu p-t-110 p-b-70 bg1-pattern">
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-lg-6 p-r-35 p-r-15-lg m-l-r-auto">
					<?php for($i = 0; $i < $numberList; $i++){ ?>
						<div class="wrap-item-mainmenu p-b-22">
							<h3 class="tit-mainmenu tit10 p-b-25">
								<?php
									if(in_array($categories[$i]['nom'], $listeCateg))
										echo $categories[$i]['nom'];
								?>
							</h3>
							<?php
								for($j = 0; $j < count($listPlats); $j++){
									if($categories[$i]['nom'] == $listPlats[$j]->nomCategorie){
							?>
								<div class="item-mainmenu m-b-36">
									<div class="flex-w flex-b m-b-3">
										<a href="#" class="name-item-mainmenu txt21">
											<?php echo $listPlats[$j]->nom;?>
										</a>
										<div class="line-item-mainmenu bg3-pattern"></div>
										<div class="price-item-mainmenu txt22">
											<?php echo number_format($listPlats[$j]->prix, 0, '.', ' ')." Ar";?>
										</div>
									</div>
								</div>
							<?php }} ?>
						</div>
					<?php } ?>
				</div>

				<div class="col-md-10 col-lg-6 p-l-35 p-l-15-lg m-l-r-auto">
					<?php for($i = $numberList; $i < count($categories); $i++){ ?>
						<div class="wrap-item-mainmenu p-b-22">
							<h3 class="tit-mainmenu tit10 p-b-25">
								<?php
									if(in_array($categories[$i]['nom'], $listeCateg))
										echo $categories[$i]['nom'];
								?>
							</h3>
							<?php
								for($j = 0; $j < count($listPlats); $j++){
									if($categories[$i]['nom'] == $listPlats[$j]->nomCategorie){	
							?>
								<div class="item-mainmenu m-b-36">
									<div class="flex-w flex-b m-b-3">
										<a href="#" class="name-item-mainmenu txt21">
											<?php echo $listPlats[$j]->nom;?>
										</a>

										<div class="line-item-mainmenu bg3-pattern"></div>

										<div class="price-item-mainmenu txt22">
											<?php echo number_format ( $listPlats[$j]->prix, 0, '.', ' ')." Ar"; ?>
										</div>
									</div>
								</div>
							<?php }} ?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</section>