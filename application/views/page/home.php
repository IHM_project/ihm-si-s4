<!-- Slide1 -->
<section class="section-slide">
	<div class="wrap-slick1">
		<div class="slick1">
			<div class="item-slick1 item1-slick1" style="background-image: url(<?php echo base_url(); ?>assets/images/slide1-01.jpg);">
				<div class="wrap-content-slide1 sizefull flex-col-c-m p-l-15 p-r-15 p-t-150 p-b-170">
					<span class="caption1-slide1 txt1 t-center animated visible-false m-b-15" data-appear="fadeInDown">
						Welcome to
					</span>

					<h2 class="caption2-slide1 tit1 t-center animated visible-false m-b-37" data-appear="fadeInUp">
						Barea Place
					</h2>

					<div class="wrap-btn-slide1 animated visible-false" data-appear="zoomIn">
						<!-- Button1 -->
						<a href="<?php echo base_url(); ?>PlatCtrl/listePlats" class="btn1 flex-c-m size1 txt3 trans-0-4">
							Look Menu
						</a>
					</div>
				</div>
			</div>

			<div class="item-slick1 item2-slick1" style="background-image: url(<?php echo base_url(); ?>assets/images/master-slides-02.jpg);">
				<div class="wrap-content-slide1 sizefull flex-col-c-m p-l-15 p-r-15 p-t-150 p-b-170">
					<span class="caption1-slide1 txt1 t-center animated visible-false m-b-15" data-appear="rollIn">
						Welcome to
					</span>

					<h2 class="caption2-slide1 tit1 t-center animated visible-false m-b-37" data-appear="lightSpeedIn">
						Barea Place
					</h2>

					<div class="wrap-btn-slide1 animated visible-false" data-appear="slideInUp">
						<!-- Button1 -->
						<a href="<?php echo base_url(); ?>PlatCtrl/listePlats" class="btn1 flex-c-m size1 txt3 trans-0-4">
							Look Menu
						</a>
					</div>
				</div>
			</div>

			<div class="item-slick1 item3-slick1" style="background-image: url(<?php echo base_url(); ?>assets/images/master-slides-01.jpg);">
				<div class="wrap-content-slide1 sizefull flex-col-c-m p-l-15 p-r-15 p-t-150 p-b-170">
					<span class="caption1-slide1 txt1 t-center animated visible-false m-b-15" data-appear="rotateInDownLeft">
						Welcome to
					</span>

					<h2 class="caption2-slide1 tit1 t-center animated visible-false m-b-37" data-appear="rotateInUpRight">
						Barea Place
					</h2>

					<div class="wrap-btn-slide1 animated visible-false" data-appear="rotateIn">
						<!-- Button1 -->
						<a href="<?php echo base_url(); ?>PlatCtrl/listePlats" class="btn1 flex-c-m size1 txt3 trans-0-4">
							Look Menu
						</a>
					</div>
				</div>
			</div>
		</div>

		<div class="wrap-slick1-dots"></div>
	</div>
</section>

<!-- Welcome -->
<section class="section-welcome bg1-pattern p-t-120 p-b-105">
	<div class="container">
		<div class="row">
			<div class="col-md-6 p-t-45 p-b-30">
				<div class="wrap-text-welcome t-center">
					<span class="tit2 t-center">
						Malagasy Restaurant
					</span>

					<h3 class="tit3 t-center m-b-35 m-t-5">
						Welcome
					</h3>

					<p class="t-center m-b-22 size3 m-l-r-auto">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper. Duis arcu massa, scelerisque vitae, consequat in, pretium a, enim. Pellentesque congue.
					</p>
				</div>
			</div>

			<div class="col-md-6 p-b-30">
				<div class="wrap-pic-welcome size2 bo-rad-10 hov-img-zoom m-l-r-auto">
					<img src="<?php echo base_url(); ?>assets/images/our-story-01.jpg" alt="IMG-OUR">
				</div>
			</div>
		</div>
	</div>
</section>