DROP TABLE IF EXISTS commande;
DROP TABLE IF EXISTS plat;
DROP TABLE IF EXISTS categoriePlat;
DROP TABLE IF EXISTS numCommande;
DROP TABLE IF EXISTS utilisateur;
DROP TABLE IF EXISTS profil;
DROP TABLE IF EXISTS tables;
DROP TABLE IF EXISTS paiement;

CREATE TABLE IF NOT EXISTS categoriePlat (
	idCategoriePlat INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(60)
);

CREATE TABLE IF NOT EXISTS paiement (
	idPaiement INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	commande INT REFERENCES commande(idCommande),
	prix INT
);

CREATE TABLE IF NOT EXISTS numCommande (
	idNumCommande INT NOT NULL PRIMARY KEY AUTO_INCREMENT
);

-- Disponibilite : 0 ==> non disponible ; 1 ==> disponible
CREATE TABLE IF NOT EXISTS plat (
	idPlat INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(60),
	categorie INT REFERENCES categoriePlat(idCategoriePlat),
	prix INT,
	disponible INT
);

CREATE TABLE IF NOT EXISTS profil (
	idProfil INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(60)
);

CREATE TABLE IF NOT EXISTS utilisateur (
	idUtilisateur INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(60),
	prenom VARCHAR(60),
	sexe CHAR(1),
	email VARCHAR(60),
	motDePasse VARCHAR(100),
	profil INT REFERENCES profil(idProfil)
);

CREATE TABLE IF NOT EXISTS tables (
	idTable INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(50)
);

CREATE TABLE IF NOT EXISTS commande (
	idCommande INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	numCommande INT REFERENCES numCommande(idNumCommande),
	serveur INT REFERENCES utilisateur(idUtilisateur),
	plat INT REFERENCES plat(idPlat),
	nombre INT,
	tables INT REFERENCES tables(idTable)
);

CREATE VIEW platDispo AS (
select 
	p.*,
	c.nom as nomCategorie 
from plat as p 
join categoriePlat as c on p.categorie = c.idCategoriePlat 
where p.disponible = 1
);

insert into categoriePlat (nom) values ("Jus");
insert into categoriePlat (nom) values ("Snack");
insert into categoriePlat (nom) values ("Entree");
insert into categoriePlat (nom) values ("Soupe");
insert into categoriePlat (nom) values ("Plat chaud");


insert into plat (nom, categorie, prix, disponible) values ('massa id', 2, 99587, 1);
insert into plat (nom, categorie, prix, disponible) values ('ut', 1, 55397, 1);
insert into plat (nom, categorie, prix, disponible) values ('eu felis', 5, 11486, 1);
insert into plat (nom, categorie, prix, disponible) values ('duis', 5, 6263, 1);
insert into plat (nom, categorie, prix, disponible) values ('vel', 2, 47857, 1);
insert into plat (nom, categorie, prix, disponible) values ('turpis donec', 2, 38342, 1);
insert into plat (nom, categorie, prix, disponible) values ('rutrum', 5, 11411, 1);
insert into plat (nom, categorie, prix, disponible) values ('enim', 1, 9425, 1);
insert into plat (nom, categorie, prix, disponible) values ('erat', 2, 50784, 1);
insert into plat (nom, categorie, prix, disponible) values ('mattis', 4, 8343, 1);
insert into plat (nom, categorie, prix, disponible) values ('in tempus', 4, 11712, 1);
insert into plat (nom, categorie, prix, disponible) values ('aliquet at', 4, 66685, 1);
insert into plat (nom, categorie, prix, disponible) values ('ultrices', 1, 38790, 1);
insert into plat (nom, categorie, prix, disponible) values ('habitasse', 4, 60263, 1);
insert into plat (nom, categorie, prix, disponible) values ('ante', 3, 25529, 1);
insert into plat (nom, categorie, prix, disponible) values ('integer ac', 5, 37436, 1);
insert into plat (nom, categorie, prix, disponible) values ('erat', 3, 37788, 1);
insert into plat (nom, categorie, prix, disponible) values ('sem sed', 2, 14545, 1);
insert into plat (nom, categorie, prix, disponible) values ('ut blandit', 4, 40533, 1);
insert into plat (nom, categorie, prix, disponible) values ('at', 1, 71544, 1);
insert into plat (nom, categorie, prix, disponible) values ('eu', 4, 27445, 1);
insert into plat (nom, categorie, prix, disponible) values ('auctor gravida', 1, 31484, 1);
insert into plat (nom, categorie, prix, disponible) values ('et ultrices', 1, 54474, 1);
insert into plat (nom, categorie, prix, disponible) values ('mauris', 1, 38667, 1);
insert into plat (nom, categorie, prix, disponible) values ('ut', 5, 20585, 1);
insert into plat (nom, categorie, prix, disponible) values ('nunc vestibulum', 3, 62627, 1);
insert into plat (nom, categorie, prix, disponible) values ('duis', 4, 81705, 1);
insert into plat (nom, categorie, prix, disponible) values ('convallis', 5, 92453, 1);
insert into plat (nom, categorie, prix, disponible) values ('id', 4, 66113, 1);
insert into plat (nom, categorie, prix, disponible) values ('elementum', 5, 47335, 1);
insert into plat (nom, categorie, prix, disponible) values ('tellus', 3, 88922, 1);
insert into plat (nom, categorie, prix, disponible) values ('pulvinar nulla', 5, 44788, 1);
insert into plat (nom, categorie, prix, disponible) values ('posuere', 2, 61163, 1);
insert into plat (nom, categorie, prix, disponible) values ('aliquam', 2, 59503, 1);
insert into plat (nom, categorie, prix, disponible) values ('eget semper', 1, 39052, 1);
insert into plat (nom, categorie, prix, disponible) values ('venenatis lacinia', 4, 29755, 1);
insert into plat (nom, categorie, prix, disponible) values ('libero', 3, 58336, 1);
insert into plat (nom, categorie, prix, disponible) values ('consequat', 5, 14837, 1);
insert into plat (nom, categorie, prix, disponible) values ('ut', 2, 95689, 1);
insert into plat (nom, categorie, prix, disponible) values ('justo lacinia', 4, 88909, 1);
insert into plat (nom, categorie, prix, disponible) values ('orci', 4, 62953, 1);
insert into plat (nom, categorie, prix, disponible) values ('nullam molestie', 3, 44919, 1);
insert into plat (nom, categorie, prix, disponible) values ('in', 2, 54405, 1);
insert into plat (nom, categorie, prix, disponible) values ('dapibus nulla', 1, 63157, 1);
insert into plat (nom, categorie, prix, disponible) values ('orci', 3, 68607, 1);
insert into plat (nom, categorie, prix, disponible) values ('ante ipsum', 1, 86926, 1);
insert into plat (nom, categorie, prix, disponible) values ('neque', 2, 64635, 1);
insert into plat (nom, categorie, prix, disponible) values ('sapien a', 3, 56933, 1);
insert into plat (nom, categorie, prix, disponible) values ('nam nulla', 4, 33068, 1);
insert into plat (nom, categorie, prix, disponible) values ('ut', 2, 96409, 1);

insert into profil (nom) values ("admin");
insert into profil (nom) values ("serveur");
insert into profil (nom) values ("serveuse");

insert into utilisateur (nom, prenom, sexe, email, motDePasse, profil) values ('Tucknutt', 'Gipsy', 'f', 'gtucknutt0@tinyurl.com', SHA1('BWjS5b8a2ZQ'), 3);
insert into utilisateur (nom, prenom, sexe, email, motDePasse, profil) values ('Twitty', 'Imogene', 'f', 'itwitty1@phpbb.com', SHA1('UJ0tDD'), 3);
insert into utilisateur (nom, prenom, sexe, email, motDePasse, profil) values ('Coughtrey', 'Allyce', 'f', 'acoughtrey2@printfriendly.com', SHA1('Uf2uSOFB3tcQ'), 3);
insert into utilisateur (nom, prenom, sexe, email, motDePasse, profil) values ('Mordy', 'Rosabelle', 'f', 'rmordy3@nyu.edu', SHA1('wUPgK1s'), 3);
insert into utilisateur (nom, prenom, sexe, email, motDePasse, profil) values ('Belbin', 'Carlene', 'f', 'cbelbin4@linkedin.com', SHA1('hczLvk61S3l'), 3);
insert into utilisateur (nom, prenom, sexe, email, motDePasse, profil) values ('Kerwen', 'Catriona', 'f', 'ckerwen5@un.org', SHA1('aGRxgZdntROg'), 3);
insert into utilisateur (nom, prenom, sexe, email, motDePasse, profil) values ('Kernocke', 'Candis', 'f', 'ckernocke6@printfriendly.com', SHA1('Yk4PJRuodjj'), 3);
insert into utilisateur (nom, prenom, sexe, email, motDePasse, profil) values ('Monahan', 'Julianna', 'f', 'jmonahan7@craigslist.org', SHA1('DCrDhEuOFQx'), 3);
insert into utilisateur (nom, prenom, sexe, email, motDePasse, profil) values ('Troubridge', 'Ray', 'f', 'rtroubridge8@mysql.com', SHA1('Adc5zU'), 3);
insert into utilisateur (nom, prenom, sexe, email, motDePasse, profil) values ('Moorton', 'Lira', 'f', 'lmoorton9@comcast.net', SHA1('BcyojwjCpu7'), 3);

insert into utilisateur (nom, prenom, sexe, email, motDePasse, profil) values ('Rawsen', 'Christiano', 'm', 'crawsen0@live.com', SHA1('bIq7lVNE'), 2);
insert into utilisateur (nom, prenom, sexe, email, motDePasse, profil) values ('Hebner', 'Emory', 'm', 'ehebner1@ucoz.ru', SHA1('XGuvAl2kl'), 2);
insert into utilisateur (nom, prenom, sexe, email, motDePasse, profil) values ('Slorance', 'Yvon', 'm', 'yslorance2@amazon.co.jp', SHA1('KZ6zoq'), 2);
insert into utilisateur (nom, prenom, sexe, email, motDePasse, profil) values ('Americi', 'Jean', 'm', 'jamerici3@ftc.gov', SHA1('zRg6bS1hCJm3'), 2);
insert into utilisateur (nom, prenom, sexe, email, motDePasse, profil) values ('Hartshorn', 'Wilfrid', 'm', 'whartshorn4@technorati.com', SHA1('lZS5QF2y'), 2);
insert into utilisateur (nom, prenom, sexe, email, motDePasse, profil) values ('MacRirie', 'Vernen', 'm', 'vmacririe5@redcross.org', SHA1('Oq4Ma2YEO'), 2);
insert into utilisateur (nom, prenom, sexe, email, motDePasse, profil) values ('De Lisle', 'Bartie', 'm', 'bdelisle6@marriott.com', SHA1('q4WXroV7QL'), 2);
insert into utilisateur (nom, prenom, sexe, email, motDePasse, profil) values ('Pynner', 'Alexio', 'm', 'apynner7@diigo.com', SHA1('xPhYfHE'), 2);
insert into utilisateur (nom, prenom, sexe, email, motDePasse, profil) values ('Eul', 'Albrecht', 'm', 'aeul8@ocn.ne.jp', SHA1('RI19TwNd6uW'), 2);
insert into utilisateur (nom, prenom, sexe, email, motDePasse, profil) values ('Plumridege', 'Gaspard', 'm', 'gplumridege9@rediff.com', SHA1('4ZQTYPire'), 2);

insert into utilisateur (nom, prenom, sexe, email, motDePasse, profil) values ('HUNALD', 'Marcus', 'm', 'marcushunald@gmail.com', SHA1('marcus'), 1);
insert into utilisateur (nom, prenom, sexe, email, motDePasse, profil) values ('Plumridege', 'Gaspard', 'm', 'rakotoarivelobarinekena@gmail.com', SHA1('bary'), 1);

insert into tables (nom) values ("Table 1");
insert into tables (nom) values ("Table 2");
insert into tables (nom) values ("Table 3");
insert into tables (nom) values ("Table 4");
insert into tables (nom) values ("Table 5");
insert into tables (nom) values ("Table 6");
insert into tables (nom) values ("Table 7");
insert into tables (nom) values ("Table 8");
insert into tables (nom) values ("Table 9");
insert into tables (nom) values ("Table 10");

